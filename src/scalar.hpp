#pragma once

#include "arch.hpp"

namespace gm {
  float reinterpret_as_float(uint32_t ui) {
      float f;
      std::memcpy(&f, &ui, 4);
      return f;
  }
  uint32_t reinterpret_as_uint32(float f) {
      uint32_t ui;
      std::memcpy(&ui, &f, 4);
      return ui;
  }

  float abs(float x) {
    float s;
    asm ("vabs.f32 %0, %1" : "=w" (s) : "w" (x) );
    return s;
  }
  float sqrt(float x) {
    float s;
    asm ("vsqrt.f32 %0, %1" : "=w" (s) : "w" (x) );
    return s;
  }
  
  /*float rsqrt_approx(float x) {
    __m128 data = _mm_set1_ps(x);
    return _mm_cvtss_f32(_mm_rsqrt_ss(data));
  }
  float rsqrt(float x) {
    float r = rsqrt_approx(x);
    float m = x * (r * r);
    return (0.5f * r) * (3.0f - m);
  }*/
  float rsqrt(float x) {
    return 1.0f/sqrt(x);
  }
}
