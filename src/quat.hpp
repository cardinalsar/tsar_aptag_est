#pragma once

#include "scalar.hpp"
#include "vec.hpp"

namespace gm {
  
class quat {
public:
  float32x4_t data;
  
  quat():data(vec4(0,0,0,1).data) {}
  quat(float _x, float _y, float _z, float _w) : data(vec4(_x,_y,_z,_w).data) {}
  quat(vec3 v, float _w) : data(vec4(v,_w).data) {}
  explicit quat(float32x4_t d) : data(d) {}
  
  
  float x() const { return _lane_extract<0>(data); }
  float y() const { return _lane_extract<1>(data); }
  float z() const { return _lane_extract<2>(data); }
  float w() const { return _lane_extract<3>(data); }
};

vec3 operator*(quat q, vec3 p) {
  float s = q.w();
  vec3 v = vec3(q.data);
  auto t = s * cross(v,p) + dot(v,p) * v;
  return p * (s*s - dot(v,v)) + (t + t);
}
quat operator*(quat a, quat b) {
  auto awww = vec3(a.w());
  vec3 bwww = vec3(b.w());
  vec3 axyz = vec3(a.data);
  vec3 bxyz = vec3(b.data);
  vec3 azxy = vec3(a.data).zxy();
  vec3 bzxy = vec3(b.data).zxy();
  vec3 ayzx = vec3(a.data).yzx();
  vec3 byzx = vec3(b.data).yzx();
  vec3 rzxy = awww*bzxy + azxy*bwww + axyz*byzx - ayzx*bxyz;
  vec4 wpart = vec4(a.data)*vec4(b.data)*vec4(1,-1,-1,-1);
  return quat(rzxy.yzx(), sum(wpart));
}

float sum(quat v) { return sum(vec4(v.data)); }
quat operator+(quat a, quat b) { return quat(vaddq_f32(a.data, b.data)); }
quat operator-(quat a, quat b) { return quat(vsubq_f32(a.data, b.data)); }
quat operator*(quat a, float b) { return quat((vec4(a.data) * vec4(b)).data); }
quat operator*(float a, quat b) { return quat((vec4(a) * vec4(b.data)).data);; }
quat operator/(quat a, float b) { return quat((vec4(a.data) * vec4(1.0f/b)).data); }
quat operator-(quat a) { return quat((-vec4(a.data)).data); }
  
float dot(quat a, quat b) { return sum(vec4(a.data) * vec4(b.data)); }
float length(quat v) { return sqrt(dot(v,v)); }
quat norm(quat v) { return v * rsqrt(dot(v,v)); }
quat conj(quat v) { return quat(-v.x(), -v.y(), -v.z(), v.w()); }
quat inv(quat v) {
  float s = 1.0/dot(v,v);
  return quat(v.x()*-s,v.y()*-s,v.z()*-s, v.w()*s);
}
quat from_axis_angle(float angle, vec3 v) {
  return quat(v * sinf(angle/2.0), cosf(angle/2.0));
}
quat shortest_arc(vec3 v1, vec3 v2) {
  return quat(cross(v1,v2), dot(v1,v1) * dot(v2,v2) + dot(v1,v2));
}
quat rotated_by(quat q, vec3 v) {
  return q + (quat(v, 0) * q)*0.5f;
}

}
