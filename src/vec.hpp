#pragma once

#include "arch.hpp"
#include "scalar.hpp"

namespace gm
{
  float get_low_f32x2(float32x2_t val) {
    float s;
    asm ("vmov.f32 %0, %1" : "=t" (s) : "x" (val));
    return s;
  }
  float get_high_f32x2(float32x2_t val) {
    float s;
    asm ("vmov.f32 %0, %p1" : "=t" (s) : "x" (val) : "d5" );
    return s;
  }

  template <size_t i>
  float _lane_extract(float32x4_t data);
  template <> float _lane_extract<0>(float32x4_t data) {
    return get_low_f32x2(vget_low_f32(data));
  }
  template <> float _lane_extract<1>(float32x4_t data) {
    return get_high_f32x2(vget_low_f32(data));
  }
  template <> float _lane_extract<2>(float32x4_t data) {
    return get_low_f32x2(vget_high_f32(data));
  }
  template <> float _lane_extract<3>(float32x4_t data) {
    return get_high_f32x2(vget_high_f32(data));
  }

  template <size_t i>
  float _lane_extract(float32x2_t data);
  template <>
  float _lane_extract<0>(float32x2_t data) { return get_low_f32x2(data); }
  template <>
  float _lane_extract<1>(float32x2_t data) { return get_high_f32x2(data); }
  
  float32x2_t vmake_f32(float _x, float _y) {
    float32x2_t val = {_x, _y};
    /*asm ("vmov %1, %0\n\
          vmov %2, %p0" : "=x" (val) : "t" (_x), "t" (_y));*/
    return val;
  }
  
  class vec2 {
  public:
    float32x2_t data;
    
    vec2(): data(vdup_n_f32(0.0f)) {}
    vec2(float _x, float _y): data(vmake_f32(_x,_y)) {}
    explicit vec2(float v): data(vdup_n_f32(v)) {}
    explicit vec2(float32x2_t d) : data(d) {}
    
    float x() const { return _lane_extract<0>(data); }
    float y() const { return _lane_extract<1>(data); }
  };
    
  // vec2 Operations
  float sum(vec2 v) { return _lane_extract<0>(vpadd_f32(v.data, v.data)); }
  vec2 operator-(vec2 a, vec2 b) { return vec2(vsub_f32(a.data,b.data)); }
  vec2 operator+(vec2 a, vec2 b) { return vec2(vadd_f32(a.data,b.data)); }
  vec2 operator*(vec2 a, vec2 b) { return vec2(vmul_f32(a.data,b.data)); }
  vec2 operator*(vec2 a, float b) { return a * vec2(b); }
  vec2 operator*(float a, vec2 b) { return b * vec2(a); }
  vec2 operator/(vec2 a, float b) { return a * vec2(1.0f/b); }
  vec2 operator-(vec2 a) { return vec2(vneg_f32(a.data)); }
  
  float dot(vec2 a, vec2 b) { return sum(a*b); }
  float length(vec2 v) { return sqrt(dot(v,v)); }
  vec2 norm(vec2 v) { return v*rsqrt(dot(v,v)); }
  vec2 abs(vec2 v) { return vec2(vabs_f32(v.data)); }
  vec2 max(vec2 a, vec2 b) { return vec2(vmin_f32(a.data, b.data)); }
  vec2 min(vec2 a, vec2 b) { return vec2(vmin_f32(a.data, b.data)); }
  vec2 perp(vec2 a) { return vec2(-a.y(), a.x()); }
  vec2 nperp(vec2 a) { return vec2(a.y(), -a.x()); }
  
  float orient2D(vec2 a, vec2 b, vec2 c) {
    vec2 u = a-c, v = b-c;
    return u.x() * v.y() - u.y() * v.x();
  }
  
  
  class vec3 {
  public:
    float32x4_t data;
    
    vec3(): data(vdupq_n_f32(0.0f)) {}
    vec3(vec2 vxy, float _z): data(vcombine_f32(vxy.data,vdup_n_f32(_z))) {}
    vec3(float _x, float _y, float _z): data(vcombine_f32(vmake_f32(_x,_y),vdup_n_f32(_z))) {}
    explicit vec3(float v): data(vdupq_n_f32(v)) {}
    explicit vec3(float32x4_t d) : data(d) {}
    
    float x() const { return _lane_extract<0>(data); }
    float y() const { return _lane_extract<1>(data); }
    float z() const { return _lane_extract<2>(data); }
    vec3 zxy() const {
      float32x2x2_t t = vtrn_f32(vget_low_f32(data),vget_high_f32(data));
      return vec3(vcombine_f32(vrev64_f32(t.val[0]), t.val[1]));
    }
    vec3 yzx() const {
      float32x2x2_t t = vtrn_f32(vrev64_f32(vget_low_f32(data)),vget_high_f32(data));
      return vec3(vcombine_f32(t.val[0], t.val[1]));
    }
  };
  vec3 test (vec3 a) { return a.yzx(); }
  // vec3 Operations
  float sum(vec3 v) { return sum(vec2(vget_low_f32(v.data))) + v.z(); }
  vec3 operator-(vec3 a, vec3 b) { return vec3(vsubq_f32(a.data,b.data)); }
  vec3 operator+(vec3 a, vec3 b) { return vec3(vaddq_f32(a.data,b.data)); }
  vec3 operator*(vec3 a, vec3 b) { return vec3(vmulq_f32(a.data,b.data)); }
  vec3 operator*(vec3 a, float b) { return a * vec3(b); }
  vec3 operator*(float a, vec3 b) { return b * vec3(a); }
  vec3 operator/(vec3 a, float b) { return a * vec3(1.0f/b); }
  vec3 operator-(vec3 a) { return vec3(vnegq_f32(a.data)); }
  
  float dot(vec3 a, vec3 b) { return sum(a*b); }
  float length(vec3 v) { return sqrt(dot(v,v)); }
  vec3 norm(vec3 v) { return v*rsqrt(dot(v,v)); }
  vec3 abs(vec3 v) { return vec3(vabsq_f32(v.data)); }
  vec3 max(vec3 a, vec3 b) { return vec3(vmaxq_f32(a.data, b.data)); }
  vec3 min(vec3 a, vec3 b) { return vec3(vminq_f32(a.data, b.data)); }
  vec3 cross(vec3 a, vec3 b) { return (a.zxy()*b - a*b.zxy()).zxy(); }
  
  float orient3D(vec3 a, vec3 b, vec3 c, vec3 d) {
    return dot(a-d,cross(b-d,c-d));
  }
  
  class vec4 {
  public:
    float32x4_t data;
    
    vec4(): data(vdupq_n_f32(0.0f)) {}
    vec4(vec2 vxy, float _z, float _w): data(vcombine_f32(vxy.data, vec2(_z, _w).data)) {}
    vec4(vec3 vxyz, float _w): data(vsetq_lane_f32(_w, vxyz.data, 3)) {}
    vec4(float _x, float _y, float _z, float _w): data(vcombine_f32(vec2(_x, _y).data, vec2(_z, _w).data)) {}
    explicit vec4(float v): data(vdupq_n_f32(v)) {}
    explicit vec4(float32x4_t d) : data(d) {}
    
    float x() const { return _lane_extract<0>(data); }
    float y() const { return _lane_extract<1>(data); }
    float z() const { return _lane_extract<2>(data); }
    float w() const { return _lane_extract<3>(data); }
  };
  
  float sum(vec4 v) { return sum(vec2(vpadd_f32(vget_high_f32(v.data),vget_low_f32(v.data)))); }
  vec4 operator-(vec4 a, vec4 b) { return vec4(vsubq_f32(a.data,b.data)); }
  vec4 operator+(vec4 a, vec4 b) { return vec4(vaddq_f32(a.data,b.data)); }
  vec4 operator*(vec4 a, vec4 b) { return vec4(vmulq_f32(a.data,b.data)); }
  vec4 operator*(vec4 a, float b) { return a * vec4(b); }
  vec4 operator*(float a, vec4 b) { return b * vec4(a); }
  vec4 operator/(vec4 a, float b) { return a * vec4(1.0f/b); }
  vec4 operator-(vec4 a) { return vec4(vnegq_f32(a.data)); }
  
  float dot(vec4 a, vec4 b) { return sum(a*b); }
  float length(vec4 v) { return sqrt(dot(v,v)); }
  vec4 norm(vec4 v) { return v*rsqrt(dot(v,v)); }
  vec4 abs(vec4 v) { return vec4(vabsq_f32(v.data)); }
  vec4 max(vec4 a, vec4 b) { return vec4(vmaxq_f32(a.data, b.data)); }
  vec4 min(vec4 a, vec4 b) { return vec4(vminq_f32(a.data, b.data)); }
  
};
