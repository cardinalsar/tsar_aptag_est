#pragma once

#include <cstdint>
#include <cstring>
#include <cmath>

//Autoselect architecture
#if __x86_64__ || _M_X64
  #define GEOMATH_ARCH_SSE
#endif
#if __ARM_NEON__ || __ARM_ARCH || _M_ARM
  #define GEOMATH_ARCH_NEON
#endif

// Include the right intrinsics header
#if defined(GEOMATH_ARCH_SSE)
  #include <xmmintrin.h>
#elif defined(GEOMATH_ARCH_NEON)
  #include <arm_neon.h>
#else
  #error "No architecture detected. Try -DGEOMATH_ARCH_SSE or -DGEOMATH_ARCH_NEON"
#endif
