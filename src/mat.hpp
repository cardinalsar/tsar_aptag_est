#pragma once

#include "arch.hpp"
#include "vec.hpp"
#include "quat.hpp"
#include "scalar.hpp"

namespace gm {
  
constexpr struct mat_identity{} identity = {};

struct row_major_array_view {
  const float* array;
};
row_major_array_view row_major(const float* f) {
  return {f};
}
struct col_major_array_view {
  const float* array;
};
col_major_array_view col_major(const float* f) {
  return {f};
}
  
class mat3 {
public:
  vec3 c0, c1, c2;
  mat3(vec3 c0, vec3 c1, vec3 c2) : c0(c0), c1(c1), c2(c2) {}
  explicit mat3(mat_identity m) : c0(1.0, 0.0, 0.0), c1(0.0, 1.0, 0.0), c2(0.0, 0.0, 1.0) {}
  explicit mat3(const row_major_array_view& m) {
    float32x2x3_t low = vld3_f32(m.array);
    c0.data = vld1q_lane_f32(m.array+6, vcombine_f32(low.val[0],low.val[0]), 2);
    c1.data = vld1q_lane_f32(m.array+7, vcombine_f32(low.val[1],low.val[1]), 2);
    c2.data = vld1q_lane_f32(m.array+8, vcombine_f32(low.val[2],low.val[2]), 2);
  }
  /*explicit mat3(const col_major_array_view& m) {
    auto low = vec4(_mm_loadu_ps(m.array+0));
    auto hi = vec4(_mm_loadu_ps(m.array+4));
    auto extra = vec4(_mm_load_ss(m.array+8));
    c0 = vec3(low.data);
    vec4 temp = {hi.x(), hi.y(), low.z(), low.w()};
    c1 = vec3(vec4(temp.w(), temp.x(), temp.y(), temp.z()).data);
    c2 = vec3(_mm_shuffle_pd(hi.data, extra.data, 1));
  }*/
};

mat3 operator+(mat3 m, mat3 n) {
  return mat3(m.c0+n.c0, m.c1+n.c1, m.c2+n.c2);
}
mat3 operator-(mat3 m, mat3 n) {
  return mat3(m.c0-n.c0, m.c1-n.c1, m.c2-n.c2);
}
mat3 operator-(mat3 m) {
  return mat3(-m.c0, -m.c1, -m.c2);
}

vec3 operator*(mat3 m, vec3 v) {
  auto t = vmulq_lane_f32(m.c0.data, vget_low_f32(v.data),0);
  t = vmlaq_lane_f32(t, m.c1.data, vget_low_f32(v.data), 1);
  t = vmlaq_lane_f32(t, m.c2.data, vget_high_f32(v.data), 0);
  return vec3(t);
}
mat3 operator*(mat3 m, mat3 n) {
  return mat3(m * n.c0, m * n.c1, m * n.c2);
}

mat3 operator*(mat3 m, float a) {
  return mat3(m.c0 * a, m.c1 * a, m.c2 * a);
}
mat3 operator*(float a, mat3 m) {
  return mat3(m.c0 * a, m.c1 * a, m.c2 * a);
}
mat3 operator/(mat3 m, float a) {
  return mat3(m.c0 / a, m.c1 / a, m.c2 / a);
}

/*
mat3 transpose(mat3 m) {
  float32x4_t r0 = m.c0.data, r1 = m.c1.data, r2 = m.c2.data, r3 = m.c2.data;
  _MM_TRANSPOSE4_PS(r0,r1,r2,r3);
  return mat3(vec3(r0), vec3(r1), vec3(r2));
}*/
/*
class mat4 {
public:
  vec4 c0, c1, c2, c3;
  mat4(vec4 c0, vec4 c1, vec4 c2, vec4 c3) : c0(c0), c1(c1), c2(c2), c3(c3) {}
  explicit mat4(mat_identity m) :
    c0(1.0, 0.0, 0.0, 0.0),
    c1(0.0, 1.0, 0.0, 0.0),
    c2(0.0, 0.0, 1.0, 0.0),
    c3(0.0, 0.0, 0.0, 1.0) {
  }
  explicit mat4(const row_major_array_view& m) {
    float32x4_t r0 = _mm_loadu_ps(m.array+0),
                r1 = _mm_loadu_ps(m.array+4),
                r2 = _mm_loadu_ps(m.array+8),
                r3 = _mm_loadu_ps(m.array+12);
    _MM_TRANSPOSE4_PS(r0, r1, r2, r3);
    c0 = vec4(r0);
    c1 = vec4(r1);
    c2 = vec4(r2);
    c3 = vec4(r3);
  }
  explicit mat4(const col_major_array_view& m) {
    c0 = vec4(_mm_loadu_ps(m.array+0));
    c1 = vec4(_mm_loadu_ps(m.array+4));
    c2 = vec4(_mm_loadu_ps(m.array+8));
    c3 = vec4(_mm_loadu_ps(m.array+12));
  }
};


mat4 operator+(mat4 m, mat4 n) {
  return mat4(m.c0+n.c0, m.c1+n.c1, m.c2+n.c2, m.c3+n.c3);
}
mat4 operator-(mat4 m, mat4 n) {
  return mat4(m.c0-n.c0, m.c1-n.c1, m.c2-n.c2, m.c3-n.c3);
}
mat4 operator-(mat4 m) {
  return mat4(-m.c0, -m.c1, -m.c2, -m.c3);
}
// implicitly tread v as homogenous, i.e. performs M * (v.x,v.y,v.z,1),
// and ignore w of result;
vec3 operator*(mat4 m, vec3 v) {
  return vec3((m.c0 * v.x() + m.c1 * v.y() + m.c2 * v.z() + m.c3).data);
}
vec4 operator*(mat4 m, vec4 v) {
  return m.c0 * v.x() + m.c1 * v.y() + m.c2 * v.z() + m.c3 * v.w();
}
mat4 operator*(mat4 m, mat4 n) {
  return mat4(m * n.c0, m * n.c1, m * n.c2, m * n.c3);
}

mat4 operator*(mat4 m, float a) {
  return mat4(m.c0 * a, m.c1 * a, m.c2 * a, m.c3 * a);
}
mat4 operator*(float a, mat4 m) {
  return mat4(m.c0 * a, m.c1 * a, m.c2 * a, m.c3 * a);
}
mat4 operator/(mat4 m, float a) {
  return mat4(m.c0 / a, m.c1 / a, m.c2 / a, m.c3 / a);
}

mat4 transpose(mat4 m) {
  float32x4_t r0 = m.c0.data, r1 = m.c1.data, r2 = m.c2.data, r3 = m.c3.data;
  _MM_TRANSPOSE4_PS(r0,r1,r2,r3);
  return mat4(vec4(r0), vec4(r1), vec4(r2), vec4(r3));
}
*/
/*
    // Sets this matrix to a projection matrix.
    // fov: the horizontal field of view (in radians)
    // aspect: width/height of the image plane.
    //   the projection is wider than tall if aspect > 1
    // near: the near clipping plane
    // far: the far clipping plane.
      float tangent = std::tan(fov/2.0f);
      mat[ 0] = 1.0f/tangent;
      mat[ 1] = 0.0f;
      void SetProjectionFOV(float fov, float aspect, float near, float far) {
      mat[ 2] = 0.0f;
      mat[ 3] = 0.0f;
      
      mat[ 4] = 0.0f;
      mat[ 5] = aspect/tangent;
      mat[ 6] = 0.0f;
      mat[ 7] = 0.0f;
      
      mat[ 8] = 0.0f;
      mat[ 9] = 0.0f;
      mat[10] = -(far + near)/(far - near);
      mat[11] = -1.0f;
      
      mat[12] = 0.0f;
      mat[13] = 0.0f;
      mat[14] = (-2.0f * far * near)/(far - near);
      mat[15] = 0.0f;
    }
    void SetProjection(float r, float t, float near, float far) {
      mat[ 0] = near/r;
      mat[ 1] = 0.0f;
      mat[ 2] = 0.0f;
      mat[ 3] = 0.0f;
      
      mat[ 4] = 0.0f;
      mat[ 5] = near/t;
      mat[ 6] = 0.0f;
      mat[ 7] = 0.0f;
      
      mat[ 8] = 0.0f;
      mat[ 9] = 0.0f;
      mat[10] = -(far + near)/(far - near);
      mat[11] = -1.0f;
      
      mat[12] = 0.0f;
      mat[13] = 0.0f;
      mat[14] = (-2.0f * far * near)/(far - near);
      mat[15] = 0.0f;
    }
    void SetLookAt(vec3 eye, vec3 at, vec3 up) {
      vec3 l = norm(at - eye);
      vec3 s = cross(l,norm(up));
      vec3 u = cross(s,l);
      l = -l;
      mat[ 0] = s.x();
      mat[ 1] = u.x();
      mat[ 2] = l.x();
      mat[ 3] = 0.0f;
      
      mat[ 4] = s.y();
      mat[ 5] = u.y();
      mat[ 6] = l.y();
      mat[ 7] = 0.0f;
      
      mat[ 8] = s.z();
      mat[ 9] = u.z();
      mat[10] = l.z();
      mat[11] = 0.0f;
      
      mat[12] = -dot(eye,s);
      mat[13] = -dot(eye,u);
      mat[14] = -dot(eye,l);
      mat[15] = 1.0f;
      
    }
    // if the matrix consists of a orthonormal 3x3, translation and
    // (0,0,0,1) lower row, sets this matrix to its inverse.
    void SetSemiInvert()
    {
      float x = mat[0] * mat[12] + mat[1] * mat[13] + mat[2] * mat[14];
      float y = mat[4] * mat[12] + mat[5] * mat[13] + mat[6] * mat[14];
      float z = mat[8] * mat[12] + mat[9] * mat[13] + mat[10]* mat[14];
      
      std::swap(mat[1], mat[4]);
      std::swap(mat[2], mat[8]);
      std::swap(mat[6], mat[9]);
      
      mat[12] = -x;
      mat[13] = -y;
      mat[14] = -z;
    }
};


inline void quat_to_mat3x3(const quat& q, float* mat)
{
  mat[ 0] = 1.0f - 2.0f * ( q.y * q.y + q.z * q.z );
  mat[ 1] = 2.0f * (q.x * q.y + q.z * q.w);
  mat[ 2] = 2.0f * (q.x * q.z - q.y * q.w);
  mat[ 3] = 2.0f * (q.x * q.y - q.z * q.w );
  mat[ 4] = 1.0f - 2.0f * (q.x * q.x + q.z * q.z );
  mat[ 5] = 2.0f * (q.z * q.y + q.x * q.w );
  mat[ 6] = 2.0f * (q.x * q.z + q.y * q.w );
  mat[ 7] = 2.0f * (q.y * q.z - q.x * q.w );
  mat[ 8] = 1.0f - 2.0f * ( q.x * q.x + q.y * q.y );
};
inline void quat_to_mat4x4(const quat& q, float* mat)
{
  mat[ 0] = 1.0f - 2.0f * ( q.y * q.y + q.z * q.z );
  mat[ 1] = 2.0f * (q.x * q.y + q.z * q.w);
  mat[ 2] = 2.0f * (q.x * q.z - q.y * q.w);
  
  mat[ 4] = 2.0f * (q.x * q.y - q.z * q.w );
  mat[ 5] = 1.0f - 2.0f * (q.x * q.x + q.z * q.z );
  mat[ 6] = 2.0f * (q.z * q.y + q.x * q.w );
  
  mat[ 8] = 2.0f * (q.x * q.z + q.y * q.w );
  mat[ 9] = 2.0f * (q.y * q.z - q.x * q.w );
  mat[10] = 1.0f - 2.0f * ( q.x * q.x + q.y * q.y );
};
*/

//special routines:

//mat4 look_at_matrix(vec3 eye, vec3 at, vec3 up);
//mat4 projectionFOV(float fov, float aspect, float near, float far);
//mat4 projection(float r, float t, float near, float far);
//mat3 rotation(quat q);
//mat4 transform(vec3 pos, quat orient);
//mat4 transform_inverse(mat4 m);
//mat4 inverse(mat4 m);
//mat3 inverse(mat3 m);

};
