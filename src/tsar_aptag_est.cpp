/**
 * tsar_aptag_est.cpp
 *
 * This is a testing file for learning about subscribing to topics and sending control messages.
 *
 * @author Sumant Sharma <sharmas@stanford.edu> and Adrien Perkins <adrienp@stanford.edu>
 */

// ros includes
#include <ros/ros.h>

// standard includes
#include <stdio.h>
#include <cstdlib>
#include <math.h>

// dji includes
#include <dji_sdk/dji_drone.h>
#include <read_cam/detection.h>
#include <tsar_aptag_est/aptag_location.h>
#include "gm.hpp"
// using the DJI onboard sdk namespace for convenience and readability
using namespace DJI::onboardSDK;

using namespace gm;

// the most recent roll, pitch and yaw values for the gimbal position (wrt the global frame)
float gimbal_roll = 0;
float gimbal_pitch = 0;
float gimbal_yaw = 0;

// make the publisher of the range, bearing data global for now
ros::Publisher loc_pub;

vec3 calcPosition(const mat3& invP, const mat3& H, float sideLength) {
  float normalizer = 1.0f/gm::sqrt(length(invP * H.c0) * length(invP * H.c1));
  if (H.c2.z() < 0)
    normalizer = -normalizer;
  return (invP * H.c2) * (normalizer * sideLength * 0.5f);
}

vec3 applyGimbalTransform(vec3 p) {

  // p is in camera space. z forward, x right, y down.
  constexpr float deg_to_rad = 3.14159f * (1/180.0f);
  float x = p.x(), y = p.y(), z = p.z();
  // apply roll to x,y
{

  float c = cosf(gimbal_roll * deg_to_rad);
  float s = sinf(-gimbal_roll * deg_to_rad);
  // rotation formula (x', y') = (c*x + s*y, c*y - s*x)
  float a = c*x + s*y;
  float b = c*y - s*x;
  x = a;
  y = b;
}

  // apply pitch to y,z
{
  float c = cosf(gimbal_pitch * deg_to_rad);
  float s = sinf(-gimbal_pitch * deg_to_rad);
  float a = c*y + s*z;
  float b = c*z - s*y;
  y = a;
  z = b;
}
  // apply yaw to x,z
  {
  float c = cosf(gimbal_yaw * deg_to_rad);
  float s = sinf(-gimbal_yaw * deg_to_rad);
  float a = c*x + s*z;
  float b = c*z - s*x;
  x = a;
  z = b;
  }
  // now z is north, x is east, and y is down. swap to get NED
  return {z,x,y};
}

/* the callback for the gimbal position */
void gimbalCallback(const dji_sdk::Gimbal::ConstPtr& gimbal) {

  // DEBUG
  //ROS_INFO("Gimbal Angles (PRY)\t%f \t%f \t%f\n", gimbal->pitch, gimbal->roll, gimbal->yaw);

  // populate this "globals" to be able to access them in the detection callback
  gimbal_roll = gimbal->roll;
  gimbal_pitch = gimbal->pitch;
  gimbal_yaw = gimbal->yaw;
}


/* the callback for the april tag detector */
void aptagEstCallback(const read_cam::detection::ConstPtr& msg) {
	//ROS_INFO("the detected april tag is at\ncx:\t%f\ncy:\t%f\n", msg->cx, msg->cy);

 	float cx = msg->cx;
	float cy = msg->cy;
	float bearing_yaw = (cx/1280.0f - 0.5f)*81.595f; // deg from center of the image horizontally
	float bearing_pitch = (cy/720.0f - 0.5f)*64.953f; // deg from center of the image vertically

  // the struct to publish
  tsar_aptag_est::aptag_location aptag_loc;

	/* approximate april tag location */
  // please leave this in, it's a good baseline benchmark to have
	float edgeLength_3D =0.39; // 0.06; //[m] This is the height of the current april tag in world coordinates
	float edgeLength_2D = std::sqrt(pow((msg->corners[2] - msg->corners[0])*((6.17 * pow(10,-3))/1280),2) + pow((msg->corners[3] - msg->corners[1])*((4.55 * pow(10,-3) )/720),2));
	float approx_range = (edgeLength_3D/edgeLength_2D) * 3.5437 * pow(10,-3) * 1.3;
	//float range = CalcRange(msg->corners,0.07938);
  
	// calculate the approx n, e coords
	float theta = (-bearing_pitch + gimbal_pitch) * M_PI/180.0f;
	float psi = (bearing_yaw + gimbal_yaw) * M_PI/180.0f;
	float r_3d = (approx_range);
	float dx = r_3d * cos(theta) * cos(psi);
	float dy = r_3d * cos(theta) * sin(psi);
	float dz = r_3d * sin(theta);

	// add the approx data to the topic message
	aptag_loc.range = approx_range;
 	aptag_loc.bearing_yaw = bearing_yaw;
 	aptag_loc.bearing_pitch = -bearing_pitch;
	aptag_loc.pos_approx[0] = dx;
	aptag_loc.pos_approx[1] = dy;
	aptag_loc.pos_approx[2] = dz;


  /* "exact" april tag location */
  float invPdata[9] = {0.0012873, 0., -0.818884, 0., 0.00128663, -0.465451, 0., 0., 1. };
  mat3 H = mat3(row_major(&msg->homography[0]));
  mat3 invP = mat3(row_major(invPdata));

  auto position = calcPosition(invP, H, 0.1f);
  //ROS_INFO("Camera: %f\t%f\t%f", position.x(), position.y(), position.z());
  position = applyGimbalTransform(position);
  ROS_INFO("World:  %f\t%f\t%f", position.x(), position.y(), position.z());

  // publish the approximate range and bearing information
  aptag_loc.timestamp = msg->timestamp;
  aptag_loc.family_size = msg->family_size;
  aptag_loc.id = msg->id;
  aptag_loc.world_pos[0] = position.x();
  aptag_loc.world_pos[1] = position.y();
  aptag_loc.world_pos[2] = position.z();

	loc_pub.publish(aptag_loc);
  
}


/* the standard main function required */
int main(int argc, char **argv) {

	// ros initializations
	ros::init(argc, argv, "aptag_listener");
	ros::NodeHandle nh;

	/* do subscriptions */
	ros::Subscriber sub = nh.subscribe("dji_sdk/gimbal", 100, gimbalCallback);					// subscribe to the gimbal position
	ros::Subscriber detection_sub = nh.subscribe("apriltag_detection", 100, aptagEstCallback);	// subscribe to the detection

	/* register publisher */
	loc_pub = nh.advertise<tsar_aptag_est::aptag_location>("aptag_location", 100);

	ROS_INFO("starting\n");

	// basically runs an indefinite loop (until ros::ok() is false) for the callbacks
	ros::spin();

	return 0;
}
